# 
# Do NOT Edit the Auto-generated Part!
# Generated by: spectacle version 0.27
# 

Name:       salmeta

# >> macros
# << macros

%{!?qtc_qmake:%define qtc_qmake %qmake}
%{!?qtc_qmake5:%define qtc_qmake5 %qmake5}
%{!?qtc_make:%define qtc_make make}
%{?qtc_builddir:%define _builddir %qtc_builddir}
Summary:    Metawatch manager application
Version:    0.1.5
Release:    1
Group:      Communications/Bluetooth
License:    GPLv3
URL:        https://gitorious.org/javispedro-jolla-misc/salmeta/
Source0:    %{name}-%{version}.tar.bz2
Source100:  salmeta.yaml
Requires:   sailfishsilica-qt5 >= 0.10.9
Requires:   sailfish-components-bluetooth-qt5
Requires:   nemo-qml-plugin-configuration-qt5
Requires:   systemd
Requires:   systemd-user-session-targets
BuildRequires:  pkgconfig(sailfishapp) >= 1.0.2
BuildRequires:  pkgconfig(Qt5Core) >= 5.2
BuildRequires:  pkgconfig(Qt5Qml)
BuildRequires:  pkgconfig(Qt5Quick)
BuildRequires:  pkgconfig(Qt5DBus)
BuildRequires:  pkgconfig(Qt5Bluetooth) >= 5.2
BuildRequires:  pkgconfig(mlite5)
BuildRequires:  pkgconfig(libiphb)
BuildRequires:  libgato-devel
BuildRequires:  qt5-qtconnectivity-qtbluetooth-devel
BuildRequires:  desktop-file-utils

%description
Salmeta is a Metawatch manager program for Sailfish devices that allows
pairing a Metawatch, forwarding notifications as well as configuring the
watch layout and widgets.


%prep
%setup -q -n %{name}-%{version}

# >> setup
# << setup

%build
# >> build pre
# << build pre

%qtc_qmake5 

%qtc_make %{?_smp_mflags}

# >> build post
# << build post

%install
rm -rf %{buildroot}
# >> install pre
# << install pre
%qmake5_install

# >> install post
# << install post

desktop-file-install --delete-original       \
  --dir %{buildroot}%{_datadir}/applications             \
   %{buildroot}%{_datadir}/applications/*.desktop

%post
# >> post
if [ "$1" -ge 1 ]; then
systemctl-user daemon-reload || :
systemctl-user restart salmeta.service || :
fi
# << post

%postun
# >> postun
if [ "$1" -eq 0 ]; then
systemctl-user stop salmeta.service || :
systemctl-user daemon-reload || :
fi
# << postun

%files
%defattr(-,root,root,-)
%{_bindir}
%{_datadir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/86x86/apps/%{name}.png
%{_libdir}/systemd/user/salmeta.service
# >> files
# << files
