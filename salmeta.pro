TARGET = salmeta

QT += qml quick dbus bluetooth

CONFIG += link_pkgconfig sailfishapp c++11
PKGCONFIG += mlite5 libiphb
LIBS += -lgato

# Remove this if QtCreator can find headers on your machine
CONFIG(debug, debug|release) : INCLUDEPATH += /usr/include/mlite5

SOURCES += src/salmeta.cpp \
    src/controller.cpp \
    src/metawatchtransport.cpp \
    src/metawatch.cpp \
    src/metawatchbletransport.cpp \
    src/metawatchbttransport.cpp \
    src/reconnecttimer.cpp \
    src/notificationmonitor.cpp \
    src/widgetinfo.cpp \
    src/widgetinfomodel.cpp \
    src/availablewidgetsmodel.cpp

HEADERS += \
    src/controller.h \
    src/metawatchtransport.h \
    src/metawatch.h \
    src/metawatchbletransport.h \
    src/metawatchbttransport.h \
    src/reconnecttimer.h \
    src/notificationmonitor.h \
    src/widgetinfo.h \
    src/widgetinfomodel.h \
    src/availablewidgetsmodel.h

OTHER_FILES += qml/salmeta.qml \
    qml/cover/CoverPage.qml \
    rpm/salmeta.changes.in \
    rpm/salmeta.spec \
    rpm/salmeta.yaml \
    translations/*.ts \
    salmeta.desktop \
    salmeta.png \
    salmeta.service \
    qml/pages/MainPage.qml \
    qml/watch/WidgetView.qml \
    qml/watch/WatchView.qml qml/watch/add_widget.png \
    qml/watch/faces/*.qml qml/watch/faces/*.png \
    qml/watch/icons/*.png \
    qml/pages/AddWidget.qml \
    qml/watch/notification.png

CONFIG += dbus
DBUS_ADAPTORS += src/org.freedesktop.Notifications.xml

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n
#TRANSLATIONS += translations/salmeta-de.ts

unit.path = /usr/lib/systemd/user/
unit.files = salmeta.service
INSTALLS += unit
