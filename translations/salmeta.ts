<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>AddWidget</name>
    <message>
        <source> (does not fit)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AvailableWidgetsModel</name>
    <message>
        <source>Watchface: 1x1 Small</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Watchface: 2x1 Horizontal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Watchface: 2x2 MetaWatch logo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Watchface: 2x2 Big numbers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Watchface: 2x2 Fish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Watchface: 2x2 Hanzi</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Not done yet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Device settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Widgets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Time format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Date format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show separator lines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Auto-backlight</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
