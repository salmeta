#include <QtCore/QRegularExpression>

#include "widgetinfo.h"

struct WidgetInfoData : public QSharedData
{
	QUrl url;
	QString desc;
	bool invert;
	short page;
	WidgetInfo::WidgetSize size;
	WidgetInfo::WidgetPosition pos;
};

WidgetInfo::WidgetInfo()
	: d(new WidgetInfoData)
{
}

WidgetInfo::WidgetInfo(const WidgetInfo &other)
	: d(other.d)
{
}

WidgetInfo::~WidgetInfo()
{
}

WidgetInfo& WidgetInfo::operator =(const WidgetInfo &other)
{
	if (this != &other) {
		d = other.d;
	}

	return *this;
}

bool WidgetInfo::valid() const
{
	return !d->url.isEmpty();
}

QUrl WidgetInfo::url() const
{
	return d->url;
}

void WidgetInfo::setUrl(const QUrl &url)
{
	d->url = url;
}

int WidgetInfo::builtinClockfaceId() const
{
	QString file = d->url.toLocalFile();
	if (file.isEmpty()) return -1;


	// I'm definitely evil:
	static const QRegularExpression re("qml/watch/faces/builtinface([0-9]+).qml$");
	QRegularExpressionMatch match = re.match(file);
	if (match.hasMatch()) {
		return match.captured(1).toInt();
	} else {
		return -1;
	}
}

QString WidgetInfo::description() const
{
	return d->desc;
}

void WidgetInfo::setDescription(const QString &desc)
{
	d->desc = desc;
}

bool WidgetInfo::invert() const
{
	return d->invert;
}

void WidgetInfo::setInvert(bool invert)
{
	d->invert = invert;
}

int WidgetInfo::page() const
{
	return d->page;
}

void WidgetInfo::setPage(int page)
{
	d->page = page;
}

WidgetInfo::WidgetSize WidgetInfo::size() const
{
	return d->size;
}

void WidgetInfo::setSize(const WidgetSize &size)
{
	d->size = size;
}

WidgetInfo::WidgetPosition WidgetInfo::position() const
{
	return d->pos;
}

void WidgetInfo::setPosition(const WidgetPosition &pos)
{
	d->pos = pos;
}
