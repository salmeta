#include <QtCore/QDebug>
#include <QtCore/QCommandLineParser>
#include <QtGui/QGuiApplication>
#include <QtGui/QFontDatabase>
#include <QtQml/QQmlComponent>
#include <QtQml/QQmlContext>
#include <QtQuick/QQuickView>
#include <sailfishapp.h>

#include "controller.h"
#include "widgetinfomodel.h"
#include "availablewidgetsmodel.h"

int main(int argc, char *argv[])
{
	QGuiApplication *app = SailfishApp::application(argc, argv);

	QCommandLineParser parser;
	parser.setApplicationDescription("MetaWatch client application");
	parser.addHelpOption();

	QCommandLineOption opt_daemon("daemon", "start background daemon instead of settings UI");
	QCommandLineOption opt_root("root", "dconf path to the settings to use", "/apps/salmeta/watchX", "/apps/salmeta/watch0");
	parser.addOption(opt_daemon);
	parser.addOption(opt_root);

	parser.process(*app);

	bool launch_daemon = parser.isSet(opt_daemon);
	QString settings_key_prefix = parser.value(opt_root);

	qmlRegisterUncreatableType<WidgetInfo>("com.javispedro.salmeta", 1, 0, "WidgetInfo",
										   "Use the models, not this");

	QFontDatabase::addApplicationFont(SailfishApp::pathTo("qml/watch/metawatch_8pt_5pxl_CAPS.ttf").toLocalFile());
	QFontDatabase::addApplicationFont(SailfishApp::pathTo("qml/watch/metawatch_8pt_5pxl_Numerals.ttf").toLocalFile());
	QFontDatabase::addApplicationFont(SailfishApp::pathTo("qml/watch/metawatch_8pt_6pxl_Numerals.ttf").toLocalFile());
	QFontDatabase::addApplicationFont(SailfishApp::pathTo("qml/watch/metawatch_8pt_7pxl_CAPS.ttf").toLocalFile());
	QFontDatabase::addApplicationFont(SailfishApp::pathTo("qml/watch/metawatch_16pt_11pxl.ttf").toLocalFile());
	QFontDatabase::addApplicationFont(SailfishApp::pathTo("qml/watch/MetaWatch-Large-16pt-Sync.ttf").toLocalFile());

	if (launch_daemon) {
		qDebug() << "Starting salmeta (daemon) with settings from" << settings_key_prefix;

		new Controller(settings_key_prefix, SailfishApp::createView());
	} else {
		QQuickView *view = SailfishApp::createView();
		view->rootContext()->setContextProperty("curSettingsPrefix", settings_key_prefix);
		view->rootContext()->setContextProperty("curWidgets", new WidgetInfoModel(settings_key_prefix));
		view->rootContext()->setContextProperty("allWidgets", new AvailableWidgetsModel);
		view->setSource(SailfishApp::pathTo("qml/salmeta.qml"));
		view->show();
	}

	return app->exec();
}
