#ifndef METAWATCHBTTRANSPORT_H
#define METAWATCHBTTRANSPORT_H

#include <QtBluetooth/QBluetoothSocket>

#include "metawatchtransport.h"

class MetaWatchBTTransport : public MetaWatchTransport
{
	Q_OBJECT
public:
	explicit MetaWatchBTTransport(const QBluetoothAddress &address, QObject *parent = 0);
	~MetaWatchBTTransport();

	bool isDeviceConnected() const;
	void sendMessage(quint8 type, quint8 options, const QByteArray &payload);

public slots:
	void connectDevice();
	void disconnectDevice();

private:
	static QByteArray encode(quint8 type, quint8 options, const QByteArray &payload);
	static bool decode(const QByteArray &msg, quint8 *type, quint8 *options, QByteArray *payload);

private slots:
	void handleSocketConnected();
	void handleSocketDisconnected();
	void handleSocketData();

private:
	QBluetoothAddress _addr;
	QBluetoothSocket *_socket;
};

#endif // METAWATCHBTTRANSPORT_H
