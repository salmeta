#ifndef AVAILABLEWIDGETSMODEL_H
#define AVAILABLEWIDGETSMODEL_H

#include <QtCore/QAbstractListModel>
#include <QtCore/QFileSystemWatcher>

#include "widgetinfo.h"

class AvailableWidgetsModel : public QAbstractListModel
{
	Q_OBJECT
public:
	explicit AvailableWidgetsModel(QObject *parent = 0);

	enum Roles {
		UrlRole = Qt::UserRole,
		DescriptionRole,
		SizeRole
	};

	QHash<int, QByteArray> roleNames() const;
	int rowCount(const QModelIndex &parent) const;
	QVariant data(const QModelIndex &index, int role) const;

signals:

public slots:
	void reload();

private:
	QFileSystemWatcher *_watcher;
	QVector<WidgetInfo> _widgets;
};

#endif // AVAILABLEWIDGETSMODEL_H
