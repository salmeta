#ifndef METAWATCH_H
#define METAWATCH_H

#include <QObject>

#include "metawatchtransport.h"
#include "widgetinfo.h"

class MetaWatch : public QObject
{
	Q_OBJECT
	Q_ENUMS(TransportType MessageTypes DeviceType WatchMode WatchProperty UiStyle)
	Q_FLAGS(WatchProperties)

public:
	enum TransportType {
		TransportBluetooth,
		TransportBluetoothLowEnergy
	};

	explicit MetaWatch(const QString &btAddr, TransportType transport, QObject *parent = 0);
	~MetaWatch();

	enum MessageTypes {
		MessageGetDeviceType = 0x01,
		MessageGetDeviceTypeResponse = 0x02,

		MessageSetVibrateMode = 0x23,

		MessageSetRealTimeClock = 0x26,

		MessageWatchPropertyOperation = 0x30,
		MessageWatchPropertyOperationResponse = 0x31,

		MessageModeChangeIndication = 0x33,

		MessageWriteLcdBuffer = 0x40,
		MessageUpdateLcdDisplay = 0x43,
		MessageLoadTemplate = 0x44,

		MessageGetBatteryStatus = 0x56,
		MessageReadBatteryStatusResponse = 0x57,

		MessageSetWidgetList = 0xA1,

		MessageSetupAccelerometer = 0xE1,
		MessageAccelerometerDataResponse = 0xE0,

		// Messages from the propietary BT stack
		MessageConnectionChange = 0xB9,
		MessageIntervalChanged = 0xBB
	};

	enum DeviceType {
		DeviceMetaWatchDigitalGen1 = 2,
		DeviceMetaWatchDigitalGen2 = 5
	};

	enum WatchMode {
		WatchModeIdle = 0,
		WatchModeNotification = 2
	};

	enum WatchProperty {
		WatchPropertyHourFormat12h = 0,
		WatchPropertyHourFormat24h = 1,
		WatchPropertyDateFormatMMDD = 0 << 1,
		WatchPropertyDateFormatDDMM = 1 << 1,
		WatchPropertyShowSeconds = 1 << 2,
		WatchPropertyShowSeparationLines = 1 << 3,
		WatchPropertyAutoBacklight = 1 << 4,

		WatchPropertyOperationRead = 1 << 7,
		WatchPropertyOperationWrite = 0 << 7
	};
	Q_DECLARE_FLAGS(WatchProperties, WatchProperty)

	enum UiStyle {
		UiGen1 = 0,
		UiGen2 = 1
	};

	bool isDeviceConnected() const;

	void setVibrateMode(bool enable, int on_duration, int off_duration, int cycles);
	void setDateTime(const QDateTime &dt);
	void configure(WatchProperties props);

	void updateDeviceType();
	void updateBatteryStatus();
	/** Switches to v2 UI, goes to idle mode */
	void updateLcdDisplay();
	/** Goes to idle mode and a specific page */
	void updateLcdDisplayPage(int page);
	/** Goes to a specific mode. */
	void updateLcdDisplayMode(WatchMode mode);
	void updateWidgetList(const QList<WidgetInfo>& widgets);

	void clearModeImage(WatchMode mode, bool black = false);
	void sendModeImage(WatchMode mode, const QImage &image);

signals:
	void connected();
	void disconnected();

	void deviceType(DeviceType type);
	void modeChange(WatchMode mode, int page);
	void batteryStatus(bool charging, int charge);

public slots:
	void connectDevice();
	void disconnectDevice();

private slots:
	void handleTransportMessage(quint8 type, quint8 options, const QByteArray &payload);

private:
	MetaWatchTransport *_transport;
};

#endif // METAWATCH_H
