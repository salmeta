#ifndef NOTIFICATIONMONITOR_H
#define NOTIFICATIONMONITOR_H

#include <QtCore/QObject>
#include <QtCore/QDateTime>
#include <QtDBus/QDBusContext>

class NotificationMonitor : public QObject, protected QDBusContext
{
	Q_OBJECT

public:
	~NotificationMonitor();

	static NotificationMonitor *instance();

signals:
	void incomingNotification(const QString &sender, const QIcon &icon, const QString &summary, int count, const QString &body, const QDateTime &dateTime);

private:
	explicit NotificationMonitor(QObject *parent = 0);

private slots:
	friend class NotificationsAdaptor;
	uint Notify(const QString &app_name, uint replaces_id, const QString &app_icon, const QString &summary, const QString &body, const QStringList &actions, const QVariantHash &hints, int expire_timeout);
};

#endif // NOTIFICATIONMONITOR_H
