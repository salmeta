#ifndef METAWATCHTRANSPORT_H
#define METAWATCHTRANSPORT_H

#include <QObject>

class MetaWatchTransport : public QObject
{
	Q_OBJECT
public:
	explicit MetaWatchTransport(QObject *parent = 0);

public:
	virtual bool isDeviceConnected() const = 0;
	virtual void sendMessage(quint8 type, quint8 options, const QByteArray &payload) = 0;

public slots:
	virtual void connectDevice() = 0;
	virtual void disconnectDevice() = 0;

signals:
	void connected();
	void disconnected();
	void messageReceived(quint8 type, quint8 options, const QByteArray &payload);
};

#endif // METAWATCHTRANSPORT_H
