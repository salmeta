#ifndef DAEMON_H
#define DAEMON_H

#include <QtCore/QObject>
#include <QtQuick/QQuickView>
#include <MDConfGroup>

#include "metawatch.h"
#include "reconnecttimer.h"
#include "notificationmonitor.h"
#include "widgetinfomodel.h"

class Controller : public QObject
{
	Q_OBJECT

	Q_PROPERTY(MetaWatch::WatchMode mode READ mode NOTIFY modeChanged)
	Q_PROPERTY(int page READ page WRITE setPage NOTIFY pageChanged)
	Q_PROPERTY(int batteryCharge READ batteryCharge NOTIFY batteryChargeChanged)
	Q_PROPERTY(bool batteryCharging READ batteryCharging NOTIFY batteryChargingChanged)

public:
	explicit Controller(const QString &settingsPrefix, QQuickView *view = 0, QObject *parent = 0);
	~Controller();

	bool isWatchConnected() const;

	MetaWatch::WatchMode mode() const;

	int page() const;
	void setPage(int page);

	int batteryCharge() const;
	bool batteryCharging() const;

signals:
	void modeChanged();
	void pageChanged();
	void batteryChargeChanged();
	void batteryChargingChanged();

private:
	void connectToDevice();
	void updateProperties();

private slots:
	void handleSettingChanged(const QString &key);
	void handleMetaWatchConnected();
	void handleMetaWatchModeChange(MetaWatch::WatchMode mode, int page);
	void handleMetaWatchBatteryStatus(bool charging, int charge);
	void handleIncomingNotification(const QString &sender, const QIcon &icon, const QString &summary, int count, const QString &body, const QDateTime &dateTime);
	void handleWidgetChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVector<int> &roles);

private:
	MDConfGroup *_settings;
	MetaWatch *_metawatch;
	ReconnectTimer *_reconnect;
	NotificationMonitor *_monitor;
	WidgetInfoModel *_widgets;

	QString _address;

	// Watch status
	MetaWatch::WatchMode _curMode;
	int _curPage;
	int _batteryCharge;
	bool _batteryCharging;
};

#endif // DAEMON_H
