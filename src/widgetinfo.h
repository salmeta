#ifndef WIDGETINFO_H
#define WIDGETINFO_H

#include <QtCore/QMetaType>
#include <QtCore/QSharedData>
#include <QtCore/QUrl>

class WidgetInfoData;

class WidgetInfo
{
	Q_GADGET
	Q_ENUMS(WidgetSize WidgetPosition)

public:
	WidgetInfo();
	WidgetInfo(const WidgetInfo &other);
	~WidgetInfo();

	WidgetInfo& operator=(const WidgetInfo &other);

	enum WidgetSize
	{
		Size1Q = 0,
		Size2QHorizontal = 1,
		Size2QVertical = 2,
		Size4Q = 3
	};

	enum WidgetPosition
	{
		PosNW = 0,
		PosNE = 1,
		PosSW = 2,
		PosSE = 3
	};

	bool valid() const;

	int builtinClockfaceId() const;

	QUrl url() const;
	void setUrl(const QUrl &url);

	QString description() const;
	void setDescription(const QString &desc);

	bool invert() const;
	void setInvert(bool invert);

	int page() const;
	void setPage(int page);

	WidgetSize size() const;
	void setSize(const WidgetSize &size);

	WidgetPosition position() const;
	void setPosition(const WidgetPosition &pos);

private:
	QSharedDataPointer<WidgetInfoData> d;
};

Q_DECLARE_METATYPE(WidgetInfo::WidgetSize)
Q_DECLARE_METATYPE(WidgetInfo::WidgetPosition)

#endif // WIDGETINFO_H
