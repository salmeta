#include <QtCore/QDebug>
#include <QtGui/QIcon>
#include <QtDBus/QDBusConnection>
#include <QtDBus/QDBusConnectionInterface>

#include <sailfishapp.h>

#include "notificationmonitor.h"
#include "notifications_adaptor.h"

static NotificationMonitor *global_monitor = 0;

namespace
{

QHash<QString, QString> icons_for_categories;

QIcon get_builtin_icon(const QString &iconName)
{
	QIcon icon;

	QString filePath = SailfishApp::pathTo("qml/watch/icons/" + iconName + ".png").toLocalFile();
	icon.addFile(filePath);

	return icon;
}

QIcon icon_for_category(const QString &category)
{
	if (icons_for_categories.isEmpty()) {
		icons_for_categories.insert("x-nemo.email", "notification-email");
		icons_for_categories.insert("x-nemo.messaging.im", "notification-sms");
		icons_for_categories.insert("x-nemo.messaging.sms", "notification-sms");
		icons_for_categories.insert("x-nemo.messaging.mms", "notification-sms");
		icons_for_categories.insert("x-nemo.social.twitter.mention", "notification-twitter");
		icons_for_categories.insert("x-nemo.social.twitter.tweet", "notification-twitter");
		icons_for_categories.insert("x-nemo.social.facebook.notification", "notification-facebook");
		icons_for_categories.insert("x-nemo.social.facebook.statuspost", "notification-facebook");
	}

	QString iconName = icons_for_categories.value(category, QString());
	if (!iconName.isEmpty()) {
		return get_builtin_icon(iconName);
	} else {
		return QIcon();
	}
}

}

NotificationMonitor::NotificationMonitor(QObject *parent) :
	QObject(parent)
{
	QDBusConnection bus = QDBusConnection::sessionBus();
	QDBusConnectionInterface *dbus = bus.interface();
	dbus->call("AddMatch",
	           "interface='org.freedesktop.Notifications',member='Notify',type='method_call',eavesdrop='true'");
	new NotificationsAdaptor(this);
	bus.registerObject("/org/freedesktop/Notifications", this);
}

NotificationMonitor::~NotificationMonitor()
{
	QDBusConnection bus = QDBusConnection::sessionBus();
	QDBusConnectionInterface *dbus = bus.interface();
	dbus->call("RemoveMatch",
	           "interface='org.freedesktop.Notifications',member='Notify',type='method_call',eavesdrop='true'");
}

NotificationMonitor *NotificationMonitor::instance()
{
	if (!global_monitor) {
		global_monitor = new NotificationMonitor;
	}
	return global_monitor;
}

uint NotificationMonitor::Notify(const QString &app_name, uint replaces_id, const QString &app_icon, const QString &summary, const QString &body, const QStringList &actions, const QVariantHash &hints, int expire_timeout)
{
	QIcon icon;

	qDebug() << "Got notification" << app_name << app_icon << summary << body;

	// Avoid sending a reply for this method call, since we've received it
	// because we're eavesdropping.
	// The actual target of the method call will send the proper reply, not us.
	Q_ASSERT(calledFromDBus());
	setDelayedReply(true);

	// If the notification mentions a specific icon, then use it.
	// But otherwise let's prefer our builtin icons.
	if (app_icon.startsWith("/")) {
		icon = QIcon(app_icon);
	} else if (app_icon.startsWith("file:")) {
		QUrl url(app_icon);
		icon = QIcon(url.toLocalFile());
	} else {
		QString category = hints.value("category").toString();
		// Let's hardcode a few categories for now..
		if (!category.isEmpty()) {
			icon = icon_for_category(category);
		}
	}

	int count = hints.value("x-nemo-item-count").toInt();
	QDateTime dateTime = hints.value("x-nemo-timestamp").toDateTime();

	if (summary.isEmpty() && body.isEmpty()) {
		// Avoid sending empty notifications to watch.
		return 0;
	}

	emit incomingNotification(app_name, icon, summary, count, body, dateTime);

	return 0;
}
