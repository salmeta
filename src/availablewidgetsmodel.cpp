#include <QtCore/QDebug>
#include <sailfishapp.h>

#include "availablewidgetsmodel.h"

AvailableWidgetsModel::AvailableWidgetsModel(QObject *parent) :
    QAbstractListModel(parent)
{
	reload();
}

QHash<int, QByteArray> AvailableWidgetsModel::roleNames() const
{
	QHash<int, QByteArray> roles;

	roles[UrlRole] = "url";
	roles[DescriptionRole] = "description";
	roles[SizeRole] = "size";

	return roles;
}

int AvailableWidgetsModel::rowCount(const QModelIndex &parent) const
{
	if (parent.isValid()) return 0;
	return _widgets.size();
}

QVariant AvailableWidgetsModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid()) return QVariant();
	const int row = index.row();
	if (row < 0 || row >= _widgets.size()) return QVariant();

	switch (role) {
	case UrlRole:
		return QVariant::fromValue<QUrl>(_widgets[row].url());
	case DescriptionRole:
		return QVariant::fromValue<QString>(_widgets[row].description());
	case SizeRole:
		return QVariant::fromValue<int>(_widgets[row].size());
	default:
		qWarning() << "Unknown role:" << role;
		return QVariant();
	}
}

void AvailableWidgetsModel::reload()
{
	beginResetModel();
	_widgets.clear();

	// Load builtin widgets
	WidgetInfo info;

	info.setUrl(SailfishApp::pathTo("qml/watch/faces/builtinface0.qml"));
	info.setDescription(tr("Watchface: 1x1 Small"));
	info.setSize(WidgetInfo::Size1Q);
	Q_ASSERT(info.builtinClockfaceId() == 0); // Ensure face ID is autodetected from passed URL
	_widgets.append(info);

	info.setUrl(SailfishApp::pathTo("qml/watch/faces/builtinface1.qml"));
	info.setDescription(tr("Watchface: 2x1 Horizontal"));
	info.setSize(WidgetInfo::Size2QHorizontal);
	Q_ASSERT(info.builtinClockfaceId() == 1);
	_widgets.append(info);

	info.setUrl(SailfishApp::pathTo("qml/watch/faces/builtinface2.qml"));
	info.setDescription(tr("Watchface: 2x2 MetaWatch logo"));
	info.setSize(WidgetInfo::Size4Q);
	Q_ASSERT(info.builtinClockfaceId() == 2);
	_widgets.append(info);

	info.setUrl(SailfishApp::pathTo("qml/watch/faces/builtinface3.qml"));
	info.setDescription(tr("Watchface: 2x2 Big numbers"));
	info.setSize(WidgetInfo::Size4Q);
	Q_ASSERT(info.builtinClockfaceId() == 3);
	_widgets.append(info);

	info.setUrl(SailfishApp::pathTo("qml/watch/faces/builtinface4.qml"));
	info.setDescription(tr("Watchface: 2x2 Fish"));
	info.setSize(WidgetInfo::Size4Q);
	Q_ASSERT(info.builtinClockfaceId() == 4);
	_widgets.append(info);

	info.setUrl(SailfishApp::pathTo("qml/watch/faces/builtinface5.qml"));
	info.setDescription(tr("Watchface: 2x2 Hanzi"));
	info.setSize(WidgetInfo::Size4Q);
	Q_ASSERT(info.builtinClockfaceId() == 5);
	_widgets.append(info);

	endResetModel();
}
