#ifndef WIDGETINFOMODEL_H
#define WIDGETINFOMODEL_H

#include <QtCore/QAbstractListModel>
#include <MDConfGroup>

#include "widgetinfo.h"

class WidgetInfoModel : public QAbstractListModel
{
	Q_OBJECT

public:
	explicit WidgetInfoModel(const QString &settingsPrefix, QObject *parent = 0);

	enum Roles {
		UrlRole = Qt::UserRole,
		InvertRole,
		PageRole,
		SizeRole,
		PositionRole
	};

	QHash<int, QByteArray> roleNames() const;
	int rowCount(const QModelIndex &parent) const;
	QVariant data(const QModelIndex &index, int role) const;

	QList<WidgetInfo> toList() const;

	Q_INVOKABLE bool widgetOverlaps(int page, WidgetInfo::WidgetPosition pos, WidgetInfo::WidgetSize size) const;

	Q_INVOKABLE int addWidget(const QUrl &url, int page, WidgetInfo::WidgetPosition pos, WidgetInfo::WidgetSize size);
	Q_INVOKABLE void removeWidget(int widgetId);

signals:

public slots:
	void reload();

private:
	int findEmptySlot();

private slots:
	void handleSettingChanged(const QString &key);

private:
	MDConfGroup *_settings;
	QVector<WidgetInfo> _widgets;
};

#endif // WIDGETINFOMODEL_H
