#ifndef RECONNECTCONTROLLER_H
#define RECONNECTCONTROLLER_H

#include <QObject>
#include <QSocketNotifier>

class ReconnectTimer : public QObject
{
	Q_OBJECT

public:
	explicit ReconnectTimer(QObject *parent = 0);
	~ReconnectTimer();

public slots:
	void scheduleNextAttempt();
	void stop();

signals:
	void tryReconnect();

private slots:
	void handleIphbActivity();

private:
	void *_iphb;
	QSocketNotifier *_notifier;
	bool _active;
	uint _counter;
};

#endif // RECONNECTCONTROLLER_H
