#ifndef METAWATCHBLETRANSPORT_H
#define METAWATCHBLETRANSPORT_H

#include <QtCore/QObject>

#include <gato/gatoperipheral.h>
#include <gato/gatouuid.h>
#include <gato/gatoservice.h>
#include <gato/gatocharacteristic.h>

#include "metawatchtransport.h"

class MetaWatchBLETransport : public MetaWatchTransport
{
	Q_OBJECT
public:
	explicit MetaWatchBLETransport(const GatoAddress &address, QObject *parent = 0);
	~MetaWatchBLETransport();

	static const GatoUUID ServiceUuid;
	static const GatoUUID InputCharacteristicUuid;
	static const GatoUUID OutputCharacteristicUuid;

	bool isDeviceConnected() const;
	void sendMessage(quint8 type, quint8 options, const QByteArray &payload);

public slots:
	void connectDevice();
	void disconnectDevice();

private:
	static QByteArray encode(quint8 type, quint8 options, const QByteArray &payload);
	static bool decode(const QByteArray &msg, quint8 *type, quint8 *options, QByteArray *payload);

private slots:
	void handleDeviceConnected();
	void handleDeviceDisconnected();
	void handleDeviceServices();
	void handleDeviceCharacteristics(const GatoService &service);
	void handleDeviceUpdate(const GatoCharacteristic &characteristic, const QByteArray &value);

private:
	GatoPeripheral *_dev;
	GatoCharacteristic _in, _out;
};

#endif // METAWATCHBLETRANSPORT_H
