#include <QtCore/QDebug>

extern "C" {
	#include <iphbd/libiphb.h>
}

#include "reconnecttimer.h"

static unsigned int num_wait_times = 8;
static unsigned short wait_times[8] = {
	2, 5, 10, 30, 2 * 60, 5 * 60, 10 * 60, 15 * 60
};

ReconnectTimer::ReconnectTimer(QObject *parent)
	: QObject(parent),
	  _iphb(iphb_open(0)),
	  _notifier(new QSocketNotifier(iphb_get_fd(_iphb), QSocketNotifier::Read, this)),
	  _active(false),
	  _counter(0)
{
	connect(_notifier, &QSocketNotifier::activated, this, &ReconnectTimer::handleIphbActivity);
}

ReconnectTimer::~ReconnectTimer()
{
	_active = false;
	_iphb = iphb_close(_iphb);
}

void ReconnectTimer::scheduleNextAttempt()
{
	_active = true;

	time_t res = iphb_wait2(_iphb, wait_times[_counter] / 2, wait_times[_counter], 0, 0);

	if (res == -1) {
		qErrnoWarning("Failed to iphb_wait");
	}
}

void ReconnectTimer::stop()
{
	_active = false;
	_counter = 0;
}

void ReconnectTimer::handleIphbActivity()
{
	iphb_discard_wakeups(_iphb);

	qDebug() << "iphb wakeup";

	if (!_active) {
		// False awakening
		return;
	}

	_active = false;
	if (++_counter > num_wait_times)
		_counter = num_wait_times;

	emit tryReconnect();
}
