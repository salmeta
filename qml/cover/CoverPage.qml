import QtQuick 2.0
import Sailfish.Silica 1.0
import org.nemomobile.configuration 1.0
import "../watch"

CoverBackground {
	ConfigurationValue {
		id: curPageSetting
		key: curSettingsPrefix + "/cur-page"
		defaultValue: 0

		onValueChanged: {
			if (watchView.curPage !== value) {
				watchView.switchToPage(value);
			}
		}
	}

	Column {
		anchors.centerIn: parent
		spacing: Theme.paddingLarge

		Item {
			width: 96 * 2
			height: 96 * 2
			anchors.horizontalCenter: parent.horizontalCenter

			WatchView {
				id: watchView
				anchors.centerIn: parent
				scale: 2
			}
		}

		Label {
			id: label
			anchors.horizontalCenter: parent.horizontalCenter
			text: "Salmeta"
		}
	}
}
