import QtQuick 2.0
import Sailfish.Silica 1.0
import Sailfish.Bluetooth 1.0
import org.nemomobile.configuration 1.0
import "../watch"

Page {
	id: page

	ConfigurationValue {
		id: deviceAddress
		key: curSettingsPrefix + "/address"
		defaultValue: "Select"
	}

	ConfigurationValue {
		id: curPageSetting
		key: curSettingsPrefix + "/cur-page"
		defaultValue: 0

		onValueChanged: {
			if (watchView.curPage !== value) {
				watchView.switchToPage(value);
			}
		}
	}

	SilicaFlickable {
		anchors.fill: parent

		PullDownMenu {
			MenuItem {
				text: qsTr("Not done yet")
			}
		}

		contentHeight: column.height

		Column {
			id: column
			width: page.width
			spacing: Theme.paddingMedium

			PageHeader {
				title: "Salmeta"
			}

			SectionHeader {
				text: qsTr("Device settings");
			}

			ValueButton {
				label: "Device"
				value: deviceAddress.value

				onClicked: {
					var dialog = pageStack.push("Sailfish.Bluetooth.BluetoothDevicePickerDialog");

					dialog.onAccepted.connect(function() {
						if (dialog.selectedDevice !== "") {
							deviceAddress.value = dialog.selectedDevice
						}
					});
				}
			}

			ComboBox {
				id: timeFormat
				label: qsTr("Time format");

				function load() {
					if (showSecondsSetting.value) {
						currentIndex = timeFormatSetting.value ? 3 : 2;
					} else {
						currentIndex = timeFormatSetting.value ? 1 : 0;
					}
				}

				ConfigurationValue {
					id: timeFormatSetting
					key: curSettingsPrefix + "/24h-mode"
					defaultValue: 0
				}

				ConfigurationValue {
					id: showSecondsSetting
					key: curSettingsPrefix + "/show-seconds"
					defaultValue: 0
				}

				Component.onCompleted: {
					load();
				}

				menu: ContextMenu {
					MenuItem {
						text: Qt.formatTime(new Date(), "hh:mm ap")
					}
					MenuItem {
						text: Qt.formatTime(new Date(), "hh:mm")
					}
					MenuItem {
						text: Qt.formatTime(new Date(), "hh:mm:ss ap")
					}
					MenuItem {
						text: Qt.formatTime(new Date(), "hh:mm:ss")
					}
				}

				onCurrentIndexChanged: {
					timeFormatSetting.value = currentIndex === 1 || currentIndex === 3;
					showSecondsSetting.value = currentIndex === 2 || currentIndex === 3;
				}
			}

			ComboBox {
				id: dateFormat
				label: qsTr("Date format");

				ConfigurationValue {
					id: dateFormatSetting
					key: curSettingsPrefix + "/ddmm-mode"
					defaultValue: 0
				}

				menu: ContextMenu {
					MenuItem {
						text: Qt.formatDate(new Date(), "M/d")
					}
					MenuItem {
						text: Qt.formatDate(new Date(), "d/M")
					}
				}

				onCurrentIndexChanged: {
					dateFormatSetting.value = currentIndex;
				}

				Component.onCompleted: {
					currentIndex = dateFormatSetting.value ? 1 : 0;
				}
			}

			TextSwitch {
				id: separators
				text: qsTr("Show separator lines");

				ConfigurationValue {
					id: separatorsSetting
					key: curSettingsPrefix + "/show-separation-lines"
					defaultValue: false
				}

				onCheckedChanged: {
					separatorsSetting.value = checked;
				}

				Component.onCompleted: {
					checked = separatorsSetting.value;
				}
			}

			TextSwitch {
				id: autobacklight
				text: qsTr("Auto-backlight");

				ConfigurationValue {
					id: autobacklightSetting
					key: curSettingsPrefix + "/auto-backlight"
					defaultValue: false
				}

				onCheckedChanged: {
					autobacklightSetting.value = checked;
				}

				Component.onCompleted: {
					checked = autobacklightSetting.value;
				}
			}


			SectionHeader {
				text: qsTr("Widgets");
			}

			Item {
				width: 96 * 4
				height: 96 * 4
				anchors.horizontalCenter: parent.horizontalCenter

				WatchView {
					id: watchView
					anchors.centerIn: parent
					scale: 4

					onCurPageChanged: {
						if (curPageSetting.value !== curPage) {
							curPageSetting.value = curPage;
						}
					}
				}
			}

			Row {
				anchors.horizontalCenter: parent.horizontalCenter
				spacing: 8

				Repeater {
					model: 4

					Rectangle {
						// TODO These should be GlassItem.
						width: 16
						height: 16
						radius: 8

						color: "white"
						opacity: watchView.curPage == index ? 0.9 : 0.3
					}
				}
			}

			SectionHeader {
				text: qsTr("Notifications");
			}

			Label {
				x: Theme.paddingLarge
				text: "TODO: List of notification types"
			}
		}
	}
}


