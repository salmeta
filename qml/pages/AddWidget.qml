import QtQuick 2.0
import Sailfish.Silica 1.0
import com.javispedro.salmeta 1.0

Page {
	id: page

	property int addToPage
	property int addToPos

	SilicaListView {
		id: allWidgetsListView
		anchors.fill: parent

		model: allWidgets

		header: PageHeader {
			title: "Add widget"
		}

		delegate : BackgroundItem {
			id: widgetDelegate
			contentHeight: Theme.itemSizeSmall

			Label {
				anchors.left: parent.left
				anchors.margins: Theme.paddingLarge
				anchors.verticalCenter: parent.verticalCenter
				text: model.description + (!widgetDelegate.enabled ? qsTr(" (does not fit)") : "")
				truncationMode: TruncationMode.Elide
				color: highlighted ? Theme.highlightColor : Theme.primaryColor
			}

			onClicked: {
				curWidgets.addWidget(model.url, addToPage, addToPos, model.size);
				pageStack.pop();
			}

			enabled: !curWidgets.widgetOverlaps(addToPage, addToPos, model.size);
		}
	}
}
