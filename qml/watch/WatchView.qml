import QtQuick 2.0

Flickable {
	id: watchView
	width: 96
	height: 96
	clip: true
	flickableDirection: Flickable.HorizontalFlick

	property int curPage: 0

	WidgetView {
		id: widgetView
		model: curWidgets
		editMode: true
		onEmptyWidgetClicked: {
			pageStack.push(Qt.resolvedUrl("../pages/AddWidget.qml"), {
							   'addToPage': page,
							   'addToPos': pos
						   });
		}
	}

	contentWidth: widgetView.width
	contentHeight: widgetView.height

	NumberAnimation {
		id: pivotAnim
		targets: watchView
		property: "contentX"
		to: curPage * watchView.width
		duration: 100
		easing.type: Easing.InOutQuad
	}

	onMovementStarted: {
		pivotAnim.stop()
	}

	onMovementEnded: {
		curPage = Math.round(watchView.contentX / watchView.width)
		pivotAnim.start()
	}

	function switchToPage(page) {
		curPage = page;
		pivotAnim.start();
	}
}
