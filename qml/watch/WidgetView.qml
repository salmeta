import QtQuick 2.0
import com.javispedro.salmeta 1.0

Rectangle {
	id: view
	width: 96*4
	height: 96

	color: "white";

	property alias model: rep.model
	property bool editMode: false

	signal emptyWidgetClicked(int page, int pos)

	Grid {
		id: editGrid
		anchors.fill: parent
		columns: 2 * 4
		rows: 2
		visible: editMode

		function _calculatePagePosFromIndex(index) {
			switch (index) {
			case 0:  return [0, 0];
			case 1:  return [0, 1];
			case 8:  return [0, 2];
			case 9:  return [0, 3];
			case 2:  return [1, 0];
			case 3:  return [1, 1];
			case 10: return [1, 2];
			case 11: return [1, 3];
			case 4:  return [2, 0];
			case 5:  return [2, 1];
			case 12: return [2, 2];
			case 13: return [2, 3];
			case 6:  return [3, 0];
			case 7:  return [3, 1];
			case 14: return [3, 2];
			case 15: return [3, 3];
			}
		}

		Repeater {
			model: 16
			Rectangle {
				width: 96 / 2
				height: 96 / 2
				Image {
					anchors.centerIn: parent
					source: "add_widget.png"
				}
				MouseArea {
					anchors.fill: parent
					onClicked: {
						var pagePos = editGrid._calculatePagePosFromIndex(index);
						view.emptyWidgetClicked(pagePos[0], pagePos[1]);
					}
				}
			}
		}
	}

	Repeater {
		id: rep

		Item {
			id: widgetItem

			x: (model.page * 96) + (model.position === WidgetInfo.PosNE || model.position === WidgetInfo.PosSE ? 96 / 2 : 0)
			y: (model.position === WidgetInfo.PosSE || model.position === WidgetInfo.PosSW ? 96 / 2 : 0)
			width: model.size === WidgetInfo.Size2QHorizontal | model.size === WidgetInfo.Size4Q ? 96 : 96 / 2
			height: model.size === WidgetInfo.Size2QVertical | model.size === WidgetInfo.Size4Q ? 96 : 96 / 2

			visible: widgetLoader.status === Loader.Ready;

			Loader {
				id: widgetLoader
				anchors.fill: parent
				source: url
			}

			MouseArea {
				anchors.fill: parent
				enabled: editMode

				onClicked: {
					curWidgets.removeWidget(index);
				}
			}
		}
	}
}
